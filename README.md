# undercall

Simple performance testing tool for hammering down one endpoint.

## Usage
```
./undercall worker_count request_count_per_worker url [curl_opts]
```
* `worker_count` is number of processes to run in parallel
* `request_count_per_worker` is number of requests each process does
  (non-parallel)
* `url` is any URL that `curl` accepts
* `curl_opts` any options given will be passed to `curl` unchanged (e.g.
  authorization)

Each run will create new temporary directory in `/tmp` in which output of each
process is stored if needed.

## Example

```
$ ./undercall 5 10 https://yahoo.com -m 3
# Preparation
Working directory: /tmp/tmp.MTG5b6OTBq-undercall
# Measurement
..................................................
done
# Metrics
## Response status breakdown
     50 301
## Average response time [s]
0.58067
## Median response time [s]
0.545590
## 95th percentile response time [s]
0.732193
## Minimum response time [s]
0.457895
## Maximum response time [s]
0.762368
## Calls per second
8.33333
```
